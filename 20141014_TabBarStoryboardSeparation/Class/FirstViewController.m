//
//  FirstViewController.m
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/14.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

+ (id)firstViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Item1" bundle:nil];
    id initialViewController = [storyboard instantiateInitialViewController];
    
    return initialViewController;
}

@end
