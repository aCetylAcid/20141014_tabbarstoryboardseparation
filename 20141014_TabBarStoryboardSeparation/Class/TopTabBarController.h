//
//  TopTabBarController.h
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/14.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"

@interface TopTabBarController : UITabBarController

@end
