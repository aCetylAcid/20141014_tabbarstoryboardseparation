//
//  SecondViewController.m
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/15.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

+ (id)secondViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Item2" bundle:nil];
    id initialViewController = [storyboard instantiateInitialViewController];
    
    return initialViewController;
}

@end
