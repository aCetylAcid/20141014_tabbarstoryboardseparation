//
//  TopTabBarController.m
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/14.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "TopTabBarController.h"

@interface TopTabBarController ()

@end

@implementation TopTabBarController

- (void)viewDidLoad
{
    NSArray *viewControllers = @[[FirstViewController firstViewController],
                                 [SecondViewController secondViewController],
                                 [ThirdViewController thirdViewController]];
    [self setViewControllers:viewControllers];
}

@end
