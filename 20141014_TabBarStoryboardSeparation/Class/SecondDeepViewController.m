//
//  SecondDeepViewController.m
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/15.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "SecondDeepViewController.h"

@interface SecondDeepViewController ()

@end

@implementation SecondDeepViewController

- (IBAction)btnTapped:(id)sender {
    if ( !UIAlertController.class ) {
        UIAlertView *alert;
        alert = [[UIAlertView alloc] initWithTitle:@"Message"
                                           message:@"Yes, I'm fine."
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil, nil];
        [alert show];
    } else {
        UIAlertController *alert;
        alert = [UIAlertController alertControllerWithTitle:@"Message"
                                                    message:@"Yes, I'm fine."
                                             preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK"
                                                         style:UIAlertActionStyleCancel
                                                      handler:nil];
        [alert addAction:okBtn];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
