//
//  ThirdViewController.m
//  20141014_TabBarStoryboardSeparation
//
//  Created by aCetylAcid on 2014/10/15.
//  Copyright (c) 2014年 PLISE.inc. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController

+ (id)thirdViewController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Item3" bundle:nil];
    id initialViewController = [storyboard instantiateInitialViewController];
    
    return initialViewController;
}

@end
